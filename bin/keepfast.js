#!/usr/bin/env node

const path = require('path')
const { program } = require('commander')
const info = require(path.join(__dirname, '..', 'package.json'))

program
  .name('keepfast')
  .version(info.version)
  .description(info.description)
  .command('init', 'initialize a default configuration file')
  .command('report [reportFile]', 'run performance tests and generate report')
  .command('upload [options] [reportFile]', 'upload report')

program.parse(process.argv)
