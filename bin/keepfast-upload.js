#!/usr/bin/env node

const path = require('path')
const axios = require('axios').default
const { program } = require('commander')
const simpleGit = require('simple-git')
const loadConfig = require('../lib/config').load
const writeReport = require('../lib/writer').write
const { wrapAction } = require('../lib/utils')

axios.defaults.baseURL = 'https://gitee.com/api/v5'
axios.interceptors.request.use((config) => {
  if (process.env.GITEE_API_ACCESS_TOKEN) {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${process.env.GITEE_API_ACCESS_TOKEN}`
  } else {
    throw new Error('access token not found')
  }
  return config
})

function detectPullRequest(config, ref) {
  const repo = config.repository
  return axios
    .get(`repos/${repo.owner}/${repo.name}/pulls`, { params: { per_page: 100 } })
    .then(({ data }) => data.find((item) => item.head.ref === ref))
}

async function uploadToCommit(config, report, commitSha) {
  const repo = config.repository.name
  const { owner } = config.repository
  const baseUrl = `repos/${owner}/${repo}`
  const commitCommentsUrl = `${baseUrl}/commits/${commitSha}/comments`

  console.log(`upload report to commit ${owner}/${repo}@${commitSha.substr(0, 7)}`)

  const [userRes, commentsRes] = await Promise.all([
    axios.get('user'),
    axios.get(commitCommentsUrl, { params: { per_page: 100 } })
  ])
  const comment = commentsRes.data.find((item) => item.user.id === userRes.data.id)

  if (comment) {
    console.log(`delete comment @${commitSha}/${comment.id}`)
    await axios.delete(`${baseUrl}/comments/${comment.id}`, { body: report })
  }
  return axios.post(commitCommentsUrl, { body: report })
}

async function uploadToPullRequest(config, report, pull) {
  const repo = config.repository.name
  const { owner } = config.repository
  const baseUrl = `repos/${owner}/${repo}/pulls`
  const pullCommentsUrl = `${baseUrl}/${pull.number}/comments`

  console.log(`upload report to pull request ${owner}/${repo}#${pull.number}`)

  const [userRes, commentsRes] = await Promise.all([
    axios.get('user'),
    axios.get(pullCommentsUrl, { params: { per_page: 100 } })
  ])
  const comment = commentsRes.data.find((item) => item.user.id === userRes.data.id)

  if (comment) {
    console.log(`delete comment #${pull.number}/${comment.id}`)
    await axios.delete(`${baseUrl}/comments/${comment.id}`)
  }
  return axios.post(pullCommentsUrl, { body: report })
}

async function upload(file) {
  if (!program.useMessage && !file) {
    throw new Error('reportFile argument should be specified')
  }

  let request
  let report
  const config = loadConfig()
  const git = simpleGit(process.cwd())
  const currentBranch = program.branch || (await git.revparse(['--abbrev-ref', 'HEAD'])).trim()
  const commitSha = (await git.revparse('HEAD')).trim()
  const pull = await detectPullRequest(config, currentBranch)

  if (program.useMessage) {
    report = program.useMessage
  } else {
    const rawReport = require(path.resolve(file))
    report = writeReport(rawReport, {
      url: `https://gitee.com/${config.repository.owner}/${config.repository.name}`
    })
  }
  if (pull) {
    request = uploadToPullRequest(config, report, pull)
  } else {
    request = uploadToCommit(config, report, commitSha)
  }
  return request.then(({ data }) => {
    console.log(data)
  }).catch((xhr) => {
    if (xhr && xhr.response && xhr.response.data) {
      throw new Error(xhr.response.data.message)
    } else {
      console.error(xhr)
    }
  })
}

program
  .option('--branch <branch>', 'bind branch to current commit')
  .option('--use-message <message>', 'use specified message to upload')
  .arguments('[reportFile]')
  .action(wrapAction(upload))
  .parse(process.argv)
