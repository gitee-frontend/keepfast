#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const { spawn, exec, execSync } = require('child_process')
const psList = require('ps-list')
const pidusage = require('pidusage')
const simpleGit = require('simple-git')
const { program } = require('commander')
const { parseArgsStringToArgv } = require('string-argv')
const { wrapAction } = require('../lib/utils')
const loadConfig = require('../lib/config').load

const cwd = process.cwd()
const maxBuildTime = 15 * 60 * 1000

// 解析构建性能报告
function parseBuildReportFile(file) {
  const report = JSON.parse(fs.readFileSync(file, 'utf-8'))
  return {
    version: report.version,
    modules: report.modules.map((m) => ({
      id: m.id,
      size: m.size,
      name: m.name,
      identifier: m.identifier,
      built: m.built,
      cacheable: m.cacheable
    })),
    assets: report.assets.map((a) => ({
      size: a.size,
      name: a.name,
      chunks: a.chunks,
      chunkNames: a.chunkNames
    })),
    chunks: report.chunks.map((c) => ({
      id: c.id,
      names: c.names,
      hash: c.hash,
      files: c.files,
      modules: c.modules.map((m) => m.identifier)
    })),
    entrypoints: report.entrypoints
  }
}

// 获取构建进程 id
async function getBuildToolPid(config) {
  if (!config.tool) {
    throw new Error(`invalid tool name: ${config.tool}`)
  }
  return new Promise((resolve, reject) => {
    let i = 0
    const timer = setInterval(async () => {
      i += 1
      // 如果 500 * 10 毫秒后还没找到进程
      if (i > 10) {
        clearInterval(timer)
        reject(new Error(`${config.tool} process was not found`))
        return
      }
      psList().then((list) => {
        let proc = null
        list.forEach((p) => {
          // 匹配：
          // webpack
          // webpack --mode production
          // .bin/webpack
          // .bin/webpack --mode production
          // 不匹配：
          // .bin/webpack-dev-server
          if (p.cmd.split(' ').some((cmd) => cmd.endsWith(`.bin/${config.tool}`) || cmd === config.tool)) {
            // 新进程的 pid 比较大，所以该进程最有可能是目标进程
            if (!proc || proc.pid < p.pid) {
              proc = p
            }
          }
        })
        if (proc) {
          resolve(proc.pid)
        }
      }).catch(reject)
    }, 500)
  })
}

// 生成构建性能报告
async function generateBuildReport(config) {
  let timer
  let report
  let maxMemoryUsage = 0
  const startTime = Date.now()
  const file = path.resolve(cwd, config.outputPath, config.reportPath)

  const runScript = () => new Promise((resolve, reject) => {
    exec(config.script, (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve()
    })
    getBuildToolPid(config).then((pid) => {
      console.log(`the pid of the build process: ${pid}`)
      timer = setInterval(() => {
        if (Date.now() - startTime > maxBuildTime) {
          process.kill(pid, 'SIGINT')
          reject(new Error(`build time exceeded limit (${maxBuildTime / 1000} s)`))
          return
        }
        pidusage(pid, (err, stats) => {
          if (err) {
            console.error(err)
            return
          }
          maxMemoryUsage = Math.max(stats.memory, maxMemoryUsage)
        })
      }, 1000)
    }).catch(reject)
  })

  try {
    if (config.beforeScript) {
      execSync(config.beforeScript, { stdio: 'inherit' })
    }
    await runScript()
    report = parseBuildReportFile(file)
  } catch (err) {
    report = {
      version: 'unknown',
      error: true,
      message: err.message,
      modules: [],
      assets: [],
      chunks: [],
      entrypoints: []
    }
  }
  report.memory = maxMemoryUsage
  clearInterval(timer)
  return report
}

// 生成 lighthouse 性能报告
async function generateLighthouseReport(config) {
  const configPath = path.join(cwd, 'lighthouse.config.js')
  const reportPath = path.join(cwd, 'lighthouse.json')
  const address = await new Promise((resolve, reject) => {
    const [cmd, ...args] = parseArgsStringToArgv(config.script)
    console.log('starting server for lighthouse')
    spawn(cmd, args)
      .on('exit', resolve)
      .on('error', reject)
      .stdout.on('data', (data) => {
        const key = 'Local:'
        const str = data.toString().split('\n').find((line) => line.includes(key))
        if (str) {
          resolve(str.substr(str.indexOf(key) + key.length + 1).trim())
        }
      })
  })

  await new Promise((resolve, reject) => {
    const url = `${address}${config.pagePath}`
    console.log(`running lighthouse to access ${url}`)
    spawn(
      'lighthouse',
      [
        url,
        fs.existsSync(configPath) ? `--config-path=${configPath}` : '',
        '--chrome-flags="--headless --no-sandbox"',
        `--locale=${config.locale}`,
        '--output=json',
        `--output-path=${reportPath}`
      ],
      {
        stdio: 'inherit'
      }
    )
      .on('exit', resolve)
      .on('error', reject)
  })
  process.kill(await getBuildToolPid(config))
  return require(reportPath)
}

// 生成构建性能差异报告
async function generateBuildDiffReport(config, options) {
  let report
  const git = simpleGit(cwd)
  const stableBranchHash = (await git.revparse(config.stableBranch)).trim()
  const currentHash = (await git.revparse('HEAD')).trim()
  const currentBranch = (await git.revparse(['--abbrev-ref', 'HEAD'])).trim()
  const finalReport = { time: new Date(), version: '', results: [] }

  async function build(branch, commit) {
    const startTime = new Date()
    await git.checkout(commit)
    console.log(`starting build for branch: ${branch}`)
    report = await generateBuildReport(config)
    report.time = new Date() - startTime
    report.branch = branch
    report.commit = commit
    if (report.message) {
      console.log(`${branch} branch build failed, it takes ${report.time / 1000} s`)
    } else {
      console.log(`${branch} branch build completed, it takes ${report.time / 1000} s`)
    }
    return report
  }

  report = await build(config.stableBranch, stableBranchHash)
  finalReport.version = report.version
  finalReport.results.push(report)
  if (stableBranchHash !== currentHash) {
    report = await build(options.branch || currentBranch, currentHash)
    finalReport.results.push(report)
  }
  await git.checkout(currentBranch)
  return finalReport
}

async function generateReport(config, options) {
  const report = {}

  if (config.build) {
    report.build = await generateBuildDiffReport(config.build, options)
  }
  if (config.lighthouse) {
    report.lighthouse = await generateLighthouseReport(config.lighthouse)
  }
  if (!options.output) {
    console.log(JSON.stringify(report, null, 2))
    return
  }
  if (!fs.existsSync(path.dirname(options.output))) {
    fs.mkdir(path.dirname(options.output), { recursive: true })
  }
  fs.writeFileSync(options.output, JSON.stringify(report, null, 2))
}

program
.option('--branch <branch>', 'bind branch to current commit')
  .arguments('[reportFile]')
  .action(wrapAction(async (output) => {
    const config = loadConfig()
    await generateReport(config, { output, branch: program.branch })
  }))
  .parse(process.argv)
