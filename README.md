# Keepfast

[![npm](https://img.shields.io/npm/v/@gitee/keepfast.svg)](https://gitee.com/gitee-frontend/keepfast/releases)
[![giteego keepfast](https://gitee.com/gitee-frontend/keepfast/badge/giteego.svg?name=Gitee%20Go&id=9639)](https://gitee.com/oschina/dashboard/projects/gitee-frontend/keepfast/giteegos/9639?branch=master)
[![Coverage Status](https://coveralls.io/repos/bitbucket/lcsoft/keepfast/badge.svg?branch=master)](https://coveralls.io/bitbucket/lcsoft/keepfast?branch=master)
[![npm](https://img.shields.io/npm/l/@gitee/keepfast.svg)](https://gitee.com/gitee-frontend/keepfast/blob/master/LICENSE)
[![gitee-release-cli](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-gitee--release-blueviolet.svg)](https://gitee.com/gitee-frontend/gitee-release-cli)

## 简介

Keepfast 是一个性能分析工具，能够分析网站的资源构建性能和页面性能，生成性能报告并提供优化建议，让性能监控更方便。

主要特性：

- 分析并生成构建性能报告，可直观的对比两个版本的构建性能，以便尽早发现构建性能问题
- 基于 [Lighthouse](https://github.com/GoogleChrome/lighthouse) 对网站进行性能测试，给出各方面性能指标和相关建议
- 预置 Gitee API 支持，能够在 Gitee 仓库中有新的提交时为其生成一个性能报告，随时跟踪性能指标和得分

## 预览

访问此页面可查看性能报告效果：

https://gitee.com/gitee-frontend/keepfast/commit/19cc7807bfedc13f67ce005d45dec8d3fec79597

## 安装

```bash
npm install -g lighthouse @gitee/keepfast
```

## 使用

### 创建配置文件

先为你的项目创建配置文件：

```bash
# 进入你的项目目录
cd /path/to/your/repo

# 初始化配置文件
keepfast init
```

然后修改 keepfast.config.js 配置文件，完善 repository 字段信息，如果你的项目是由 [Vue CLI](https://cli.vuejs.org/) 创建和管理的，剩下的配置内容保持原样即可。

### 生成报告

运行性能测试并将报告输出到 ./report.json 文件中：

```bash
keepfast report ./report.json
```

### 上传报告

上传报告会调用 [Gitee API](https://gitee.com/api/v5/swagger)，为了让 keepfast 有权限操作你的仓库，你需要提供一个私人令牌，它可在[设置 > 私人令牌](https://gitee.com/profile/personal_access_tokens/new) 页面中生成，生成后运行以下命令添加环境变量：

```bash
export GITEE_API_ACCESS_TOKEN=你的私人令牌
```

之后运行以下命令上传报告：

```bash
keepfast upload ./report.json
```

如果当前分支已有对应的 Pull Request，则会将报告上传到该 Pull Request 的评论中，否则上传到当前 Commit 详情页的评论中。

### 在 Gitee Go 中使用

Gitee Go 是 Gitee 推出的 CI/CD(持续构建与集成)服务，用户可以通过自定义构建流程，实现构建集成自动化。

在你的仓库的 master 分支上新建流水线文件，使用如下配置：

```yml
version: '1.0'
name: keepfast
displayName: 前端构建性能分析
triggers:
  trigger: auto
  pr:
    branches:
      include:
        - master
variables:
  global:
    - KEEPFAST_API_TOKEN
stages:
  - name: keepfast
    displayName: 前端构建性能分析
    strategy: naturally
    trigger: auto
    steps:
      - step: build@nodejs
        name: keepfast
        displayName: keepfast
        nodeVersion: 14.16.0
        commands:
          - set -e
          - export GITEE_API_ACCESS_TOKEN=$KEEPFAST_API_TOKEN
          - npm install -g yarn lighthouse @gitee/keepfast
          - keepfast upload --branch $GITEE_SOURCE_BRANCH --use-message "已开始分析，请稍后..."
          - yum install -y epel-release procps chromium
          - yum groupinstall -y "Development Tools"
          - npm install
          - keepfast report --branch $GITEE_SOURCE_BRANCH ./report.json
          - keepfast upload --branch $GITEE_SOURCE_BRANCH ./report.json
        artifacts:
          - name: BUILD_ARTIFACT
            path:
              - ./report.json
        caches:
          - ~/.npm
          - ~/.yarn
          - /var/cache/yum
```

然后在仓库的环境变量管理页面中设置 `KEEPFAST_API_TOKEN` 环境变量的值为你的访问令牌。

## 常见问题

**为什么我要选择 Keepfast，而不是 lighthouse-ci 等之类的工具？**

如果你符合以下条件则可以试试 Keepfast。

- 有多个长期开发的前端项目，对它们的构建性能和页面加载性能有较高的要求，需要自动化进行性能监控
- 是 Gitee 企业版付费用户，且前端项目都托管在 GItee 上，需要一个能在 Gitee Go 服务中使用的性能监控工具
- 懒得花时间去调研和体验同类工具，用 Keepfast 你只需要照着这文档复制粘贴跑命令即可快速体验

**跟 xxxx 比起来功能太少了！**

如果你对性能分析这方面有不错的想法和建议，可以[反馈给我们](https://gitee.com/gitee-frontend/keepfast/issues)。

## 许可

[GPLv3](LICENSE)
