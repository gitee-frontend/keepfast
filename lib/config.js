const path = require('path')
const extend = require('extend')

const defaults = {
  repository: {
    owner: 'yourname',
    name: 'example'
  },
  build: {
    tool: 'vue-cli-service',
    beforeScript: '',
    script: 'npm run build -- --report-json',
    outputPath: 'dist',
    reportPath: 'report.json',
    stableBranch: 'master'
  },
  lighthouse: {
    tool: 'vue-cli-service',
    script: 'npm run serve -- --mode production',
    locale: 'zh',
    pagePath: ''
  },
  upload: {
    writer: {
      name: 'gitee',
      entrypoints: [],
      performance: {
        maxEntrypointSize: 244 * 1024,
        maxBuildTime: 2 * 60 * 1000,
        maxBuildMemory: 1 * 1024 * 1024 * 1024
      }
    }
  }
}

const filename = 'keepfast.config.js'

function load(configPath = path.join(process.cwd(), filename)) {
  try {
    return extend(true, {}, defaults, require(configPath))
  } catch (_err) {
    throw new Error('Keepfast Couldn\'t find a configuration file.')
  }
}

module.exports = {
  filename,
  defaults,
  load
}
