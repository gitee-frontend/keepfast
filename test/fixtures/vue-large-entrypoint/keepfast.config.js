module.exports = {
  "repository": {
    "owner": "gitee-frontend",
    "name": "keepfast"
  },
  "build": {
    "tool": "vue-cli-service",
    "script": "npm run build -- --report-json",
    "outputPath": "dist",
    "reportPath": "report.json",
    "stableBranch": "master"
  },
  "lighthouse": {
    "tool": "vue-cli-service",
    "script": "npm run serve -- --mode production",
    "pagePath": ""
  },
  "upload": {
    "writer": {
      "name": "gitee",
      "entrypoints": [],
      "performance": {
        "maxBuildTime": 5000
      }
    }
  }
}
