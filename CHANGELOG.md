# [0.8.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.7.0...v0.8.0) (2022-07-27)


### Bug Fixes

* 运行子命令时报错未找到模块 (#I1M3OM) ([6d01383](https://gitee.com/gitee-frontend/keepfast/commits/6d01383e23149f07f0e0ab9101e265e51ec4ee68)), closes [#I1M3](https://gitee.com/gitee-frontend/keepfast/issues/I1M3)


### Features

* **upload:** 发新评论时删除旧评论 ([6f482a1](https://gitee.com/gitee-frontend/keepfast/commits/6f482a1271841405c8276a07a99ef5ff41db7bad))



# [0.7.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.7.0-beta...v0.7.0) (2022-07-18)


### Bug Fixes

* 处理添加 branch 选项后的分支切换问题 ([be18cdb](https://gitee.com/gitee-frontend/keepfast/commits/be18cdb87d0c5a3a3c95541b6246413f8a525ec0))



# [0.7.0-beta](https://gitee.com/gitee-frontend/keepfast/compare/v0.6.2...v0.7.0-beta) (2022-07-18)


### Bug Fixes

* 构建进程识别错误 ([7f39259](https://gitee.com/gitee-frontend/keepfast/commits/7f3925929f9becfbf1ab4bc0fdb0c579a9783de7))
* 运行全局安装的 wepack 时无法检测到进程 id ([b4747f4](https://gitee.com/gitee-frontend/keepfast/commits/b4747f4b886b13726db279e4ba252ee8e082af6a))
* **writer:** 删除分支链接中的多余花括号 ([79adcd8](https://gitee.com/gitee-frontend/keepfast/commits/79adcd8cca2b9a445e5b2d1d0ec0d02263b8baf5))


### Features

* 增加 branch 选项 ([cccac48](https://gitee.com/gitee-frontend/keepfast/commits/cccac4818456f7ef8d50310311d60f3f6c0d0630))
* add config.beforeScript ([5b383a0](https://gitee.com/gitee-frontend/keepfast/commits/5b383a0abeaeb9ab75e88e473d7c58ec812717ba))



## [0.6.2](https://gitee.com/gitee-frontend/keepfast/compare/v0.6.1...v0.6.2) (2020-08-18)


### Bug Fixes

* report file loaded incorrectly ([d10ef63](https://gitee.com/gitee-frontend/keepfast/commits/d10ef630841eb368adcd1493f55e5e367a2018c8))
* table cell data is not highlighted correctly ([074b72d](https://gitee.com/gitee-frontend/keepfast/commits/074b72d0d22de6a891783e3c338d476714c43e86))



## [0.6.1](https://gitee.com/gitee-frontend/keepfast/compare/v0.6.0...v0.6.1) (2020-07-17)



# [0.6.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.5.3...v0.6.0) (2020-07-01)


### Features

* add upload.writer.performance configuration ([1e602bc](https://gitee.com/gitee-frontend/keepfast/commits/1e602bcfbb9f2e26e60c67dcf74c6534e219c62f))



## [0.5.3](https://gitee.com/gitee-frontend/keepfast/compare/v0.5.2...v0.5.3) (2020-06-30)


### Bug Fixes

* `ps aux` not work on Windows ([2241531](https://gitee.com/gitee-frontend/keepfast/commits/2241531644a4d7589def3e430d819243f55580c6))
* the upload error was not captured correctly ([0bf4f06](https://gitee.com/gitee-frontend/keepfast/commits/0bf4f06ebdf0ec6f478ff39435a3554f0dae0c24))



## [0.5.2](https://gitee.com/gitee-frontend/keepfast/compare/v0.5.1...v0.5.2) (2020-06-29)


### Bug Fixes

* the process exit code was not set when an error occurred ([8ff6245](https://gitee.com/gitee-frontend/keepfast/commits/8ff62454901bb7501dad65ba4af153189d1722fb))



## [0.5.1](https://gitee.com/gitee-frontend/keepfast/compare/v0.5.0...v0.5.1) (2020-06-29)


### Bug Fixes

* cannot use null as a configuration property value ([7c5b77a](https://gitee.com/gitee-frontend/keepfast/commits/7c5b77a3cad788c4567b6d026d123f981a45c75c))



# [0.5.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.4.0...v0.5.0) (2020-06-28)


### Features

* add lighthouse.locale config ([8473eca](https://gitee.com/gitee-frontend/keepfast/commits/8473eca5b47bc468ed3f18e9d4c007eb6134593f))
* set the default lighthouse report language to Chinese ([771f1ac](https://gitee.com/gitee-frontend/keepfast/commits/771f1ace8a857eba2dc277bf8a2aac0aca932a81))



# [0.4.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.3.0...v0.4.0) (2020-06-24)


### Features

* add --use-message option ([78776b9](https://gitee.com/gitee-frontend/keepfast/commits/78776b90c1e47798bf155f78250b5e1179bb3fbd))



# [0.3.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.2.1...v0.3.0) (2020-06-23)


### Features

* add build time limit ([ef9d242](https://gitee.com/gitee-frontend/keepfast/commits/ef9d2421b8ef399585b9f0a8d4d3498b0a3eb57b))



## [0.2.1](https://gitee.com/gitee-frontend/keepfast/compare/v0.2.0...v0.2.1) (2020-06-22)


### Bug Fixes

* entrypoints config not work ([85a72ee](https://gitee.com/gitee-frontend/keepfast/commits/85a72ee8b238c6052e773b4c28afea8ae38ebf8f))
* remove ineffective --commit-path option ([dfd48f4](https://gitee.com/gitee-frontend/keepfast/commits/dfd48f4cfe12394b19cc04c1e7bdf3cb4296cc41))



# [0.2.0](https://gitee.com/gitee-frontend/keepfast/compare/v0.1.3...v0.2.0) (2020-06-19)


### Bug Fixes

* 'keepfast-init' does not exist ([092d40e](https://gitee.com/gitee-frontend/keepfast/commits/092d40e8940b85c277b9484b1f47553c87c4c23a))
* build time incorrect ([2e9c2a0](https://gitee.com/gitee-frontend/keepfast/commits/2e9c2a071d6c25b806a541a102520a5c49f2e724))


### Features

* add --commit-path option ([8fd54d2](https://gitee.com/gitee-frontend/keepfast/commits/8fd54d251ba03e7be489b49eca5cff7df2fdf434))
* add build performance suggests ([fa759f5](https://gitee.com/gitee-frontend/keepfast/commits/fa759f526c9fb551a35b2cdfd1698116254e80f9))



## [0.1.3](https://gitee.com/gitee-frontend/keepfast/compare/v0.1.2...v0.1.3) (2020-06-18)


### Bug Fixes

* cannot read property 'results' of undefined ([81620c5](https://gitee.com/gitee-frontend/keepfast/commits/81620c5edc3fe8fc0fa1c3bf9894306940363c06))



## [0.1.2](https://gitee.com/gitee-frontend/keepfast/compare/v0.1.1...v0.1.2) (2020-06-18)


### Bug Fixes

* lighthouse not work ([fd342dc](https://gitee.com/gitee-frontend/keepfast/commits/fd342dcc62cd16e2b034114ac71cdf9815118f25))



## [0.1.1](https://gitee.com/gitee-frontend/keepfast/compare/v0.1.0...v0.1.1) (2020-06-18)


### Bug Fixes

* config.tool undefined ([9439117](https://gitee.com/gitee-frontend/keepfast/commits/9439117c6ef68b787a78d1565e5a846ef0feea8e))



# 0.1.0 (2020-06-18)



